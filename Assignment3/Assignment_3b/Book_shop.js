var app = angular.module("shopping", []);

//First control definition
app.controller("techBookCtrl", function ($scope, $rootScope) {

    //Initializing a rootscope
    $rootScope.status = true;

    //Initalizing a json object containing the detials of the technical book
    $scope.technicalBooks = {
        'Physics': ['Nuclear Physics', 'Mechanics', 'Gravity Force'],
        'Chemistry': ['Molecules', 'Energy Bonding', 'Nuclear Chemistry'],
        'Maths': ['Algebra', 'Numerical methods', 'Differential equations']
    };

    //Returns the rootscope status that controls the checkout button
    $scope.checkStatus = function () {        
        return ($rootScope.status);
    };

    //When button is pressed,it disables the button by changing the value of rrotscope
    $scope.checkOut = function () {
        $rootScope.status = false;
    };  

    //Initialising the first book whenever the category is changed 
    $scope.changeBook = function () {
        $scope.bookName = $scope.BookCategory[0];

    };
});

//Second Control definition
app.controller("nonTechBookCtrl", function ($scope, $rootScope) {

   //Initalizing a json object containing the detials of the Non technical book
    $scope.nontechnicalBooks= {
        'Movies':['Harry Potter', 'Lord of the Rings', 'Game of Thrones'],
        'Sports': ['Cricket', 'Football', 'Baseball'],
        'Magazine': ['Daily Mail', 'News Today', 'The Hindu']
    };

   //Returns the rootscope status that controls the checkout button
    $scope.checkStatus = function () {
        return ($rootScope.status);
    };

   //When button is pressed,it disables the button by changing the value of rrotscope
    $scope.checkOut = function () {
        $rootScope.status = false;
    };   

    //Initialising the first book whenever the category is changed 
    $scope.changeBook = function () {
        $scope.bookName = $scope.BookCategory[0];
    };
});