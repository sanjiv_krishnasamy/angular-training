 var app = angular.module("shopping",[]);

//Assigning the value of STATUS in factory
        app.factory("standard", function(){
            return {
                status: true
            };
        });

//First control definition
        app.controller("techBookCtrl",function ($scope,standard) {
            
            //Initalizing a json object containing the detials of the technical book
            $scope.technicalBooks={
                    'Physics':['Nuclear Physics','Mechanics','Gravity Force'],
                    'Chemistry': ['Molecules','Energy Bonding','Nuclear Chemistry'],
                    'Maths': ['Algebra','Numerical methods','Differential equations']
            };
            
            //Returns the standard status that controls the checkout button
            $scope.checkStatus = function(){
                return (standard.status);
            };
            
            //When button is pressed,it disables the button by changing the value of standard.status
            $scope.checkOut = function (){
                standard.status = false;
            };  
            
            //Initialising the first book whenever the category is changed
            $scope.changeBook = function() {
                $scope.bookName = $scope.BookCategory[0];

            };
        });

//Second Control definition
       app.controller("nonTechBookCtrl",function ($scope,standard) {
           
           //Initalizing a json object containing the detials of the non technical book
            $scope.nontechnicalBooks={
                    'Movies':['Harry Potter','Lord of the Rings','Game of Thrones'],
                    'Sports': ['Cricket','Football','Baseball'],
                    'Magazine': ['Daily Mail','News Today','The Hindu']
            };
           
           //Returns the standard status that controls the checkout button
           $scope.checkStatus = function(){
                return (standard.status);
            };
           
           //When button is pressed,it disables the button by changing the value of standard.status
            $scope.checkOut = function (){
                 standard.status = false;
            };   
            
            //Initialising the first book whenever the category is changed
            $scope.changeBook = function() {
                $scope.bookName = $scope.BookCategory[0];
            };
        });