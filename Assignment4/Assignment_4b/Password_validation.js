angular.module("password", [])

    //Definition of a controller
    .controller("passwordCtrl", function ($scope) {
        $scope.$watch('user.password', function (newval, oldval) {
            //Checking for the special charcters in the input value
            var arr = /[\W,a-z]/.test(newval);
            if (arr) {
                $scope.status = true;
            } else {
                $scope.status = false;
            }
        });
    });