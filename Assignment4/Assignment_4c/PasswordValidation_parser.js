angular.module("password", [])

    //Definition of a directive
    .directive("checkpassword", function () {
        return {
            require: 'ngModel',
            link: function (scope, ele, att, ctl) {
                ctl.$parsers.unshift(isvalid);
                ctl.$formatters.unshift(isvalid);
                function isvalid(viewvalue) {
                    //Checking for the special charcters in the input value
                    var arr = /[\W,a-z]/.test(viewvalue);
                    if (arr) {
                        ctl.$setValidity('checkpassword', false);
                    } else {
                        ctl.$setValidity('checkpassword', true);
                    }
                }
            }
        };
    });
