var app = angular.module("studentApp", []);

//Definition of a StudentController
app.controller("StudentController", function ($scope) {
    "use strict";
    $scope.type = "firstName";
    $scope.searchText = {
        firstName : "",
        lastName : ""
    };
    
    //Student list containing the first and last name of the student
    $scope.studentList = [
        {firstName : "Sanjiv", lastName: "Krishnasamy"},
        {firstName : "Vignesh", lastName: "Sakthivel"},
        {firstName : "Suthir", lastName: "Ram"},
        {firstName : "Suraj", lastName: "Dev"},
        {firstName : "Shylesh", lastName: "Sekar"},
        {firstName : "Kevin", lastName: "Arnold"}
    ];
    
    //Watch function to toggle between two types
    $scope.$watch("type", function (newval, oldval) {
        if (!newval) {
            return;
        }
        if (newval === "lastName") {
            $scope.name = $scope.searchText.firstName;
            $scope.searchText = {};
            $scope.searchText.lastName = $scope.name;
        } else {
            $scope.name = $scope.searchText.lastName;
            $scope.searchText = {};
            $scope.searchText.firstName = $scope.name;
        }
    });
});