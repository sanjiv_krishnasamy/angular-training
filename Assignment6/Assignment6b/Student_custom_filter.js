var app = angular.module("studentApp", []);

//Definition of a StudentController
app.controller("StudentController", function ($scope) {
    "use strict";
    
    //Student list containing the first and last name of the student
    $scope.studentList = [
        {firstName : "Sanjiv", lastName: "Krishnasamy"},
        {firstName : "Vignesh", lastName: "Sakthivel"},
        {firstName : "Suthir", lastName: "Ram"},
        {firstName : "Suraj", lastName: "Dev"},
        {firstName : "Shylesh", lastName: "Sekar"},
        {firstName : "Kevin", lastName: "Arnold"}
    ];
});

//Custom filter
app.filter('searchFor', function () {
    "use strict";
    return function (array, searchString) {
        if (!searchString) {
            return array;
        }
        var result = [];
        searchString = searchString.toLowerCase();
        angular.forEach(array, function (student) {
            if (student.firstName.toLowerCase().indexOf(searchString) >= 0 || student.lastName.toLowerCase().indexOf(searchString) >= 0) {
                result.push(student);
            }
        });
        return result;
    };
});