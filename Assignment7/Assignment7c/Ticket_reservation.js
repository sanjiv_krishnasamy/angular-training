var app = angular.module("reservationApp", ['ngRoute']);


//"journeyInfo" Factory that returns the object containing the complete information about the journey
app.factory("journeyInfo", function () {
    "use strict";
    return {
        source : "",
        destination : "",
        date : "",
        busName : "",
        busType : "",
        departingTime : "",
        arivingTime : "",
        passengerName : "",
        passengerAge : "",
        address : "",
        seatsRequired : ""
    };
});

//Config part to route the html files
app.config(["$routeProvider", function ($routeProvider) {
    "use strict";
    $routeProvider
        .when('/', {
            templateUrl : "Templates/places.html",
            controller : "placeCtrl"
        })
        .when('/buses', {
            templateUrl : "Templates/buses.html",
            controller : "busCtrl"
        })
        .when('/bill', {
            templateUrl : "Templates/bill.html",
            controller : "billCtrl"
        });
    
}]);

//Place controller that controls the places.html
app.controller("placeCtrl", function ($scope, $http, $location, journeyInfo) {
    "use strict";
    
    //Used to set tha current date
    $scope.minDate = new Date();
    
    //Gets the locations.json file
    $http.get("Datas/Loactions/Locations.json")
        .success(function (response) {
            $scope.locations = response.locations;
        });
    
    //Gets called when user searches for bus. Informations are stored in factory elements
    $scope.submitLocation = function () {
        journeyInfo.source = $scope.sourceLocation.source;
        journeyInfo.destination = $scope.destinationLocation;
        journeyInfo.date = $scope.date;
        $location.path("buses");
    };
});

app.controller("busCtrl", function ($scope, $http, $location, journeyInfo) {
    "use strict";
    
    //Array containing the number of seats
    $scope.seats = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    
    //Gets the busInformation.json file
    $http.get("Datas/BusesInformation/BusesInformation.json")
        .success(function (response) {
            $scope.buses = response.buses;
        });
    
    //Gets called when user checks for availability. Informations are stored in factory elements
    $scope.checkAvailability = function (bus) {
        $scope.bus = bus;
        journeyInfo.busName = bus.name;
        journeyInfo.busType = bus.Type;
        journeyInfo.arivingTime = bus.ArivingTime;
        journeyInfo.departingTime = bus.DepartingTime;
    };
    
    //Gets called when user submits the query. Informations are stored in factory elements
    $scope.submitInformation = function (passenger) {
        journeyInfo.passengerName = passenger.passengerName;
        journeyInfo.passengerAge = passenger.passengerAge;
        journeyInfo.address = passenger.address;
        journeyInfo.seatsRequired = passenger.seatsRequired;
        $location.path("bill");
    };
});

//Definition of a billCtrl that handles the bill generation
app.controller("billCtrl", function ($scope, journeyInfo) {
    "use strict";
    $scope.info = journeyInfo;
});