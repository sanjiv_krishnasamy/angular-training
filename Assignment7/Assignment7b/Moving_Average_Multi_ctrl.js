var app = angular.module("MovingAverageapp", []);

//Parent Controller definition
app.controller("parentCtrl", function ($scope, $q, $timeout) {
    "use strict";
    
    //Object containing the data,period and an empty array
    $scope.Mov_avg = {
        datas : [1, 2, 3, 4, 5, 6, 7, 8, 9],
        period : 3,
        Avg_array : []
    };
    $scope.result_array = [];
    
    //Function that is called when body is loaded
    var loadall = function () {
        
        //Initializing promise
        var defer = $q.defer();
        var promise = defer.promise;
        
        
        promise.then(function () {
            var i, j;
            for (i in $scope.Mov_avg.datas) {
                var sum = 0;
                $scope.Mov_avg.Avg_array.push($scope.Mov_avg.datas[i]);
                if ($scope.Mov_avg.Avg_array.length > $scope.Mov_avg.period) {
                    $scope.Mov_avg.Avg_array.splice(0, 1);
                }
                for (j in $scope.Mov_avg.Avg_array) {
                    sum += $scope.Mov_avg.Avg_array[j];
                }
                $scope.result_array.push(sum / $scope.Mov_avg.Avg_array.length);
            }
            
            //Using a Broadcast to pass the array to the child controller
            $scope.$broadcast('averagesArray', {array : $scope.result_array});
        });
        
        //Resolving the promise
        $timeout(function () {
            defer.resolve();
        }, 1000);
    };
    loadall();
});

//Child Controller Definition
app.controller("childCtrl", function ($scope) {
    "use strict";
    $scope.childArray = [];
    
    //Receiving the array from the parent controller using $on
    $scope.$on('averagesArray', function (event, args) {
        $scope.childArray = args.array;
    });
});