angular.module("validate", [])
    .controller("emailCtrl", function ($scope) {
        //When submit button is clicked
        $scope.submitmail = function () {
            $scope.count = 0;
        };

        //When any change occurs in the textbox
        $scope.hidestatus = function () {
            $scope.count = 1;
        };
    });