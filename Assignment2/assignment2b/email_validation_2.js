angular.module("emailapp", [])
    .controller("emailCtrl", function ($scope) {
        //Declaring the array of objects: items
        $scope.items = [];

        //Adding the email ids to the Table
        $scope.add = function (input) {
            $scope.count = 0;
            $scope.items.push({ 
                email : input
            });
            $scope.inputs = "";

            // To make the form Dirt free
            $scope.form.$setPristine();
            $scope.form.$setUntouched();
        };

        //When any change occurs in the textbox
        $scope.hidestatus = function () {
            $scope.count = 1;
        };
    });