angular.module("Mobileapp", [])
.controller("mobileCtrl",function ($scope) {
    $scope.items = [
        {
            name : 'Micromax canvas HD',
            image : 'images/micromax.png',
            camera : '5 mp',
            gps : 'Yes with A-Gps',
            wifi : '802.11 b/g/n'
        },
        {
            name : 'Micromax canvas spark',
            image : 'images/spark.png',
            camera : '3 mp',
            gps : 'Yes',
            wifi : '802.22'
        },
        {
            name : 'Samsung Galaxy S',
            image : 'images/samsung.png',
            camera : '8 mp',
            gps : 'Yes with A-Gps',
            wifi : '802.22, DLNA'
        }
    ];
});