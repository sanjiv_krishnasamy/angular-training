var app = angular.module("bookapp", []);

//Definition of service that holds the collection of books
app.service("BookCollectionService", function () {
    "use strict";
    var books = this;
    books.list = [
        {name: 'Mechanics', author: 'Morgan', price: 1000},
        {name: 'Thermodynamics', author: 'Clarke', price: 2000}
    ];
    books.isFound = false;
});

//Definition of a Factory that can access service
app.factory("BookCollectionFactory", function (BookCollectionService) {
    "use strict";
    var i, BookCollectionFactory = {};
    
    //Searches and return the book from the collection. If not, returns the "notFound" string
    BookCollectionFactory.checkForBook = function (bookToBeSearched) {
        BookCollectionService.isFound = false;
        for (i in BookCollectionService.list) {
            if (BookCollectionService.list[i].name.toLowerCase() === bookToBeSearched.toLowerCase()) {
                BookCollectionService.isFound = true;
                return BookCollectionService.list[i];
            } 
        }
        if (!BookCollectionService.isFound) {
            return ("notFound");
        }
    };
    
    //Adds the Book to the collection and return its value
    BookCollectionFactory.addBookToCollection = function (bookToBeAdded) {
        var newBook = {name: bookToBeAdded.name, author: bookToBeAdded.author, price: bookToBeAdded.price};
        BookCollectionService.list.push(newBook);
        for (i in BookCollectionService.list) {
            if (BookCollectionService.list[i].name === bookToBeAdded.name) {
                BookCollectionService.isFound = true;
                return BookCollectionService.list[i];
            } 
        }
    };
    return BookCollectionFactory;
});

//Definition of a controller that can access Factory
app.controller("BookController", function ($scope, BookCollectionFactory) {
    "use strict";
    var books = BookCollectionFactory;
    $scope.getDetails = false;
    $scope.showDetails = false;
    
    //Search for the book.If available it enables the display, else it will display the update form
    $scope.searchBook = function () {
        $scope.getDetails = false;
        $scope.showDetails = false;
        $scope.userBook = {};
        
        // To make the form Dirt free
        $scope.addForm.$setPristine();
        $scope.addForm.$setUntouched();
        
        //Calls the function from the factory
        $scope.book = books.checkForBook($scope.bookName);
        if ($scope.book === "notFound") {
            $scope.getDetails = true;
        } else {
            $scope.showDetails = true;
        }
    };
    
    //Adds the book to the Collection and displays it to the user
    $scope.addBook = function () {
        $scope.userBook.name = $scope.bookName;
        $scope.book = books.addBookToCollection($scope.userBook);
        $scope.getDetails = false;
        $scope.showDetails = true;
    };
});