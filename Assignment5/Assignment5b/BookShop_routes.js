angular.module("bookapp", ['ngRoute'])

    .service("BookCollection", function () {
        var books = this;
    
        //Technical Book collection in array of objects
        books.technical = [
            {name: 'Mechanics', author: 'Morgan', price: 1000, available: true, stock: 2},
            {name: 'Thermodynamics', author: 'Clarke', price: 2000, available: true, stock: 1},
            {name: 'Nuclear Theory', author: 'Smith', price: 3000, available: true, stock: 0}
        ]; 
    
        //Non Technical Book collection in array of objects
        books.nontechnical = [
            {name: 'Sports', author: 'Watson', price: 1500, available: true, stock: 4},
            {name: 'Fire and Ice', author: 'Mitchell', price: 2600, available: true, stock: 2},
            {name: 'Life of pi', author: 'Brendon', price: 1200, available: true, stock: 0},
            {name: 'Einstein Theory', author: 'Frank', price: 3500, available: true, stock: 4}
        ]; 
    })

//Defining the directive
    .directive("bookcollection", function () {
        return {
            restrict: 'E',
            templateUrl: 'Books.html' 
        };
    })

//Defining the config to route to respective category
    .config(["$routeProvider", function ($routeProvider) {
        $routeProvider
            .when("/technical", {
                templateUrl: 'Routes/TechnicalBooks.html',
                controller: 'techBookctrl'
            })
            .when("/nontechnical", {
                templateUrl: 'Routes/NonTechnicalBooks.html',
                controller: 'nonTechBookctrl'
            });
    }])
//Defining the controller "Bookctrl"
    .controller("techBookctrl", function ($scope, BookCollection) {

        //Technical Book collection in array of objects
        $scope.technical = BookCollection.technical;
        
        //Function to change the stock value and value of a "add to cart" button
        $scope.toggletechnical = function (inputname) {
            var i;
            for (i in $scope.technical)
                {
                    if ($scope.technical[i].name === inputname) {
                        if ($scope.technical[i].available === true) {
                            $scope.technical[i].stock -= 1;
                            $scope.technical[i].available = $scope.technical[i].available === false ? true : false;
                        } else {
                                $scope.technical[i].stock += 1;
                                $scope.technical[i].available = $scope.technical[i].available === false ? true : false;
                            }
                    }
                }
        };   

        //Check for the availability of stock
        $scope.checkstocktechnical = function (inputname) {
            var i;
            for(i in $scope.technical) {
                if ($scope.technical[i].name === inputname) {
                    if ($scope.technical[i].stock === 0) {
                        return true;
                    } else {
                            return false;
                        }
                }
            }
        }; 
    })
    
    .controller("nonTechBookctrl", function ($scope, BookCollection) {
        //Non Technical Book collection in array of objects
        $scope.nontechnical = BookCollection.nontechnical; 
    
        //Function to change the stock value and value of a "add to cart" button
        $scope.togglenontechnical = function (inputname) {
            var i;
            for (i in $scope.nontechnical) {
                if ($scope.nontechnical[i].name === inputname) {
                    if ($scope.nontechnical[i].available === true) {
                        $scope.nontechnical[i].stock -= 1;
                        $scope.nontechnical[i].available = $scope.nontechnical[i].available === false ? true : false;
                    } else {
                        $scope.nontechnical[i].stock += 1;
                        $scope.nontechnical[i].available = $scope.nontechnical[i].available === false ? true : false;
                    }
                }
            }
        };
        
        //Check for the availability of stock
        $scope.checkstocknontechnical = function (inputname) {
            var i;
            for (i in $scope.nontechnical) {
                if ($scope.nontechnical[i].name === inputname) {
                    if ($scope.nontechnical[i].stock === 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        };

    });